#!/bin/bash

# Copyright 2018 @Coffee:matrix.org. This program is licensed under the
# GNU Affero General Public License v3.0 or later (AGPL-3.0-or-later).
# For the full text of the license, see LICENSE.txt.


# Clean out most whole index files which could be recreated from diffs.
# But do keep some originals at regular checkpoint intervals.

failed ()
{
	echo "Failed" "$@" 1>&2
	exit 1
}

here="${0%/*}"
[ "$here" = "$0" ] && here="."
cd "$here" || failed to change to \""$here"\"

. common.bash || failed to import common.bash
. config.bash || failed to import config.bash

try cd "$TARGET_DIR"

last_prefix=
delete=
for diff in *.diff.jar; do
	orig="${diff%%.diff.jar}.jar"
	[ "$diff" = "$orig" ] && continue
	[ ! -f "$orig" ] && continue

	debug "$orig"
	prefix="${orig:0:KEEP_CHARACTERS}"
	debug "$prefix"

	if [ "$last_prefix" = "$prefix" ]; then
		[ -n "$delete" ] && try rm "$delete"
		delete="$orig"
		debug delete="$orig"
	else
		msg Keeping "$orig"
	fi

	last_prefix="$prefix"
done
