#!/bin/bash

# Copyright 2018 @Coffee:matrix.org. This program is licensed under the
# GNU Affero General Public License v3.0 or later (AGPL-3.0-or-later).
# For the full text of the license, see LICENSE.txt.


failed ()
{
	echo "Failed" "$@" 1>&2
	exit 1
}

here="${0%/*}"
[ "$here" = "$0" ] && here="."
cd "$here" || failed to change to \""$here"\"

. common.bash || failed to import common.bash
. config.bash || failed to import config.bash

try mkdir -p "$TARGET_DIR"
etag_file="$TARGET_DIR"/etag
index_tmp="$TARGET_DIR"/index.tmp

etag=""; [ -f "$etag_file" ] && etag="$( cat "$etag_file" )"
debug "etag     : \"$etag\""
test_etag="$( curl -sIm 60 "$REPO/$INDEXFILE" | awk '/^[Ee][Tt][Aa][Gg]:/ { sub("^.+:[ \t]*", ""); gsub("\r", ""); print; exit 0 }' | sha1sum | awk '{ print $1 }' )"
debug "test_etag: \"$test_etag\""

[ -n "$test_etag" ] || failed to get etag from server.

if [ "$test_etag" = "$etag" ]; then
	msg "No change"
	exit 10
fi

try curl -sm 1800 "$REPO/$INDEXFILE" > "$index_tmp"
try jarsigner -verify -strict -keystore keystore "$index_tmp" fdroid
try mv "$index_tmp" "$TARGET_DIR/$( date +"$ARCHIVE_TEMPLATE" )"

echo "$test_etag" > "$etag_file"
