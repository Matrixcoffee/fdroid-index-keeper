#!/bin/bash

# Copyright 2018 @Coffee:matrix.org. This program is licensed under the
# GNU Affero General Public License v3.0 or later (AGPL-3.0-or-later).
# For the full text of the license, see LICENSE.txt.


failed ()
{
	echo "Failed" "$@" 1>&2
	exit 1
}

here="${0%/*}"
[ "$here" = "$0" ] && here="."
cd "$here" || failed to change to \""$here"\"

. common.bash || failed to import common.bash
. config.bash || failed to import config.bash

try cd "$TARGET_DIR"

target="$1"
neworig="${target%%.diff.jar}.jar"

[ ! -e "$target" ] && error "No such file: \"$TARGET_DIR/$target\""
[ -e "$neworig" ] && error "Silly person! \"$neworig\" already exists!"
[ "$target" = "${target%%.diff.jar}" ] && error "Not a diff: \"$target\""

for f in *.jar; do
	[ "$f" = "${f%%.diff.jar}" ] && root="$f"
	[ "$target" = "$f" ] && break
done

[ -n "$root" ] || error Could not find a non-diff for "$target"

clean_unpack_area rebuild
mkdir rebuild

( cd rebuild && jar x ) < "$root"

( tr ' ' '\n'; echo ) < rebuild/index-v1.json > rebuild/index-v1.nl.json

go=
for diff in *.jar; do
	[ "$diff" = "$root" ] && go=yes
	[ "$diff" = "${diff%%.diff.jar}" ] && continue
	[ -z "$go" ] && continue

	echo "$diff"
	( cd rebuild && jar x ) < "$diff"
       	try patch rebuild/index-v1.nl.json rebuild/index-v1.nl.ed
	< rebuild/index-v1.nl.json head -c -1 | tr '\n' ' ' > rebuild/index-v1.json

	jar cMf rebuild.jar -C rebuild index-v1.json -C rebuild META-INF
       	jarsigner -verify -strict -keystore ../keystore rebuild.jar fdroid \
	 || failed to verify.

	if [ "$diff" = "$target" ]; then
		try mv -n rebuild.jar "$neworig"
		clean_unpack_area rebuild
		break
	fi
done
