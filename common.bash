#!/dev/null

# Copyright 2018 @Coffee:matrix.org. This program is licensed under the
# GNU Affero General Public License v3.0 or later (AGPL-3.0-or-later).
# For the full text of the license, see LICENSE.txt.


debug ()
{
	(( quiet <= 0 )) && echo "$@" 1>&2
}

msg ()
{
	(( quiet <= 1 )) && echo "$@" 1>&2
}

emsg ()
{
	(( quiet <= 2 )) && echo "$@" 1>&2
}

error ()
{
	emsg "Error:" "$@"
	exit 1
}

failed ()
{
	emsg "Failed" "$@"
	exit 1
}

try ()
{
	msg "> $*"
	"$@" || failed to execute \""$@"\"
}

clean_unpack_area ()
{
	# safer alternative for rm -rf

	if [ -d "$1" ]; then
		if [ -d "$1"/META-INF ]; then
			rm -f "$1"/META-INF/*.{MF,SF,RSA}
			try rmdir "$1"/META-INF
		fi
		rm -f "$1"/index-v1.json \
		      "$1"/index-v1.nl.ed \
		      "$1"/index-v1.nl.json \
		      "$1"/index-v1.nl.json.rebuilt
		try rmdir "$1"
	fi
}

quiet=1

while [ "$1" ]; do case "$1" in
	-d|--debug)	(( quiet-- ))
			shift
			;;
	-q|--quiet)	(( quiet++ ))
			shift
			;;
	--)		shift
			break
			;;
	*)		break;
			;;
esac; done

