#!/bin/sh

# Copyright 2018 @Coffee:matrix.org. This program is licensed under the
# GNU Affero General Public License v3.0 or later (AGPL-3.0-or-later).
# For the full text of the license, see LICENSE.txt.


# "Anchor" or "pin" the index to a particular signer. Extracts the
# certificate from an already downloaded index, and adds it to the
# keystore for verification of subsequent downloads.

failed ()
{
	echo "Failed" "$@" 1>&2
	exit 1
}

here="${0%/*}"
[ "$here" = "$0" ] && here="."
cd "$here" || failed to change to \""$here"\"

. common.bash || failed to import common.bash
. config.bash || failed to import config.bash

index_tmp="$TARGET_DIR"/index.tmp

exec keytool -printcert -jarfile "$index_tmp" -rfc | keytool -importcert -keystore keystore -alias fdroid -noprompt -keypass fdroid -storepass fdroid
