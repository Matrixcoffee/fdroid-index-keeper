#!/bin/bash

# Copyright 2018 @Coffee:matrix.org. This program is licensed under the
# GNU Affero General Public License v3.0 or later (AGPL-3.0-or-later).
# For the full text of the license, see LICENSE.txt.


failed ()
{
	echo "Failed" "$@" 1>&2
	exit 1
}

here="${0%/*}"
[ "$here" = "$0" ] && here="."
cd "$here" || failed to change to \""$here"\"

. common.bash || failed to import common.bash
. config.bash || failed to import config.bash

unpack_and_augment_jarfile ()
{
	local jarfile="$1" \
	      dest="$2"

	local v=v; (( quiet >= 1 )) && v=

	clean_unpack_area "$dest"
	try mkdir -p "$dest"
	( cd "$dest" && jar -x$v ) < "$jarfile"
	( try tr ' ' '\n' && echo ) \
		< "$dest/index-v1.json" \
		> "$dest/index-v1.nl.json"
}

generate_diff ()
{
	local old="$1" \
	      new="$2"

	# generate the actual diff
	diff -e "$old"/index-v1.nl.json \
	        "$new"/index-v1.nl.json \
	      > "$new"/index-v1.nl.ed

	# get the old version
	try cp "$old"/index-v1.nl.json \
	       "$new"/index-v1.nl.json.rebuilt

	# try to apply the patch
	try patch "$new"/index-v1.nl.json.rebuilt \
	          "$new"/index-v1.nl.ed

	# verify the patched file
	try cmp "$new"/index-v1.nl.json \
	    "$new"/index-v1.nl.json.rebuilt

	# remove original + verification artifacts
	try rm "$new"/index-v1.nl.json.rebuilt \
	       "$new"/index-v1.json \
	       "$new"/index-v1.nl.json
}

pack_jar ()
{
	local jarfile="$1" \
	      rootdir="$2"

	rm -f "$TARGET_DIR"/jartmp
	try jar -cMf "$TARGET_DIR"/jartmp -C "$rootdir" .
	try mv "$TARGET_DIR"/jartmp "$jarfile"
}

try mkdir -p "$TARGET_DIR/unpack"

prev=

for orig in "$TARGET_DIR"/*.jar; do
	[ "$orig" != "${orig%%.diff.jar}" ] && continue
	debug "$orig"

	diff="${orig%%.jar}.diff.jar"
	debug "$diff"

	if [ -e "$diff" -o -z "$prev" ]; then
		prev="$orig"
		continue
	fi

	debug old="$prev"
	debug new="$orig"
	unpack_and_augment_jarfile "$prev" "$TARGET_DIR"/unpack/old
	unpack_and_augment_jarfile "$orig" "$TARGET_DIR"/unpack/new

	generate_diff "$TARGET_DIR"/unpack/old \
		      "$TARGET_DIR"/unpack/new

	pack_jar "$diff" "$TARGET_DIR"/unpack/new

	prev="$orig"
done

clean_unpack_area "$TARGET_DIR"/unpack/old
clean_unpack_area "$TARGET_DIR"/unpack/new
rmdir "$TARGET_DIR"/unpack
