#!/dev/null

# Copyright 2018 @Coffee:matrix.org. This program is licensed under the
# GNU Affero General Public License v3.0 or later (AGPL-3.0-or-later).
# For the full text of the license, see LICENSE.txt.


# URL of the F-Droid compatible repository to get index files from
REPO="https://f-droid.org/repo"

# Name of the index file on the server (don't change this)
INDEXFILE="index-v1.jar"

# Where to store the actual archive. A relative path will be relative to the
# location of the scripts.
TARGET_DIR="index"

# What to name the individual index files. This is passed to "date" to turn it
# into a concrete filename. See "man date" or "date --help" for the format.
# Change with care. The scripts rely on properly sortable filenames.
ARCHIVE_TEMPLATE="index-v1-%Y-%m-%d-%a-%H%M.jar"

# Number of filename characters to consider while cleaning. If there is a
# change in the first 'n' characters of the file name, keep the next original
# file. (Otherwise, only keep the diffs.)
KEEP_CHARACTERS=16
