# fdroid-index-keeper

[fdroid-index-keeper](https://gitlab.com/Matrixcoffee/fdroid-index-keeper) is a collection of scripts for keeping a historical archive of the [F-Droid](https://f-droid.org) app index.

## Why does it exist?

That's a good question. So far, as a maintainer of [TWIF](https://f-droid.org/en/2018/04/27/this-week-in-fdroid.html) having access to accurate historical indexes lets me compare them, and understand what _really_ changed.

## Status

**Alpha**. I just wrote this today.

## Requirements

```
# apt-get install git bash coreutils curl ed patch \
>                 diffutils openjdk-8-jdk-headless
```

## Recommended Installation

```
$ git clone https://gitlab.com/Matrixcoffee/fdroid-index-keeper.git
```

You can just run straight out of the git repo, or copy the scripts somewhere. Whatever you like. The only requirement is that all scripts be kept in the same directory so they can find their dependencies.

For security, it would be good if the script location and the scripts themselves weren't writable, once the certificate is pinned.

## How to use

* `config.bash`

  Edit this.

* `poll.bash`

  Run this manually once. You'll get a verification error. See below.

  When you get it working, just put it in your crontab or something, and run it at an interval you feel comfortable with.

* `anchor.bash`

  So you tried to poll, and you got a verification error. Run this script once to "anchor" or "pin" the certificate, which will be used to verify the downloaded index file from now on.

* `diffcompress.bash`

  Generate diffs between index files. Run this however often you like. Daily, weekly, or even monthly. This script is quite heavy on CPU, so keep that in mind.

* `clean.bash`

  Delete original files for which a diff exists. Again, run it however often you like. This will delete _most_ originals but not all of them. The first file is obviously kept, otherwise there's nothing to apply the diffs to. The last file is also kept, so it is possible to generate diffs for newly downloaded files.

  It will also keep an original file at regular checkpoints, so rebuilding an index file doesn't require applying a million diffs. The default configuration should keep every first index of the month.

* `rebuild.bash`

  Reconstitute any index file. Just pass the name of the diff as an argument (no path, just the basename), and the original should be rebuilt from diffs, finding the nearest original, applying however many diffs as necessary on top, zipping it back up and finally verifying the signature before putting it back at its original filename.

That should hopefully do you. Good luck!

## Caveats

* The present diff algorithm assumes the index file does not contain even a single newline character. This is currently true. Signature verification will break if this stops being true.

* Diff compression relies on the sort order of index filenames, such that lexicographical sorting also results in a chronological sort order.

## Contributing
Anyone can contribute to the project. Please send [MR](https://gitlab.com/help/user/project/merge_requests/index.md)s, or open an issue on the [tracker](https://gitlab.com/Matrixcoffee/fdroid-index-keeper/issues).

## License

Copyright 2018 [@Coffee:matrix.org](https://matrix.to/#/@Coffee:matrix.org)

   > This program is free software: you can redistribute it and/or modify
   > it under the terms of the GNU Affero General Public License as published by
   > the Free Software Foundation, either version 3 of the License, or
   > (at your option) any later version.

   > This program is distributed in the hope that it will be useful,
   > but WITHOUT ANY WARRANTY; without even the implied warranty of
   > MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   > GNU Affero General Public License for more details.

   > You should have received a copy of the GNU Affero General Public License
   > along with this program.  If not, see <https://www.gnu.org/licenses/>.

([AGPL-3.0-or-later](https://spdx.org/licenses/AGPL-3.0-or-later.html))
